//
//  ViewController.swift
//  AB-Inbev-Test
//
//  Created by Marcio Izar Bastos de Oliveira on 28/08/20.
//  Copyright © 2020 Marcio Izar Bastos de Oliveira. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tabBar: UITabBar!
    
    let productsData = ProductsData()
    var products: [Product] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        products = productsData.getAllProducts()
        
        tabBar.selectedItem = tabBar.items![0]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProductsTableViewCell
        if let image : UIImage = UIImage(named: products[indexPath.row].id) {
            cell.productImageView.image = image
        }
        cell.nameLabel.text = products[indexPath.row].name
        cell.quantityLabel.text = products[indexPath.row].quantityPerItem
        cell.volumeLabel.text = products[indexPath.row].volume
        
        if let oldPrice = products[indexPath.row].oldPrice {
            cell.priceLabel.text = "RD$" + String(format: "%.2f", products[indexPath.row].price)
            cell.priceLabel.textColor = .systemGreen
            let oldPriceWithCurrency = "RD$" + String(format: "%.2f", oldPrice)
            cell.oldPriceLabel.attributedText = oldPriceWithCurrency.strikeThrough()
            cell.oldPriceLabel.text = oldPriceWithCurrency
        } else {
            cell.priceLabel.text = "RD$" + String(format: "%.2f", products[indexPath.row].price)
            cell.priceLabel.textColor = .black
            cell.oldPriceLabel.text = ""
        }
        return cell
    }

}
extension String {
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0,attributeString.length))
        return attributeString
    }
}
