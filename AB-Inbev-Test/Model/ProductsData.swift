//
//  ProductData.swift
//  AB-Inbev-Test
//
//  Created by Marcio Izar Bastos de Oliveira on 28/08/20.
//  Copyright © 2020 Marcio Izar Bastos de Oliveira. All rights reserved.
//

import Foundation

class ProductsData {

    static var products = [
        Product(id: "1", name: "Ron Barceló - Aged rum", quantityPerItem: "1x1 Unidade por Caja", volume: "705ML CAN", price: 380.20, oldPrice: 480.00),
        Product(id: "2", name: "Ron Barceló Imperial", quantityPerItem: "1x1 Unidade por Caja", volume: "600ML CAN", price: 603.90),
        Product(id: "3", name: "Barceló - Rom Dominicano", quantityPerItem: "1x24 Unidades por Caja", volume: "700ML CAN", price: 980.80),
    ]
    
    func getAllProducts() -> [Product] {
        return ProductsData.products
    }
}
