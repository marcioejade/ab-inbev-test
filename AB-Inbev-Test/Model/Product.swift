//
//  Product.swift
//  AB-Inbev-Test
//
//  Created by Marcio Izar Bastos de Oliveira on 28/08/20.
//  Copyright © 2020 Marcio Izar Bastos de Oliveira. All rights reserved.
//

import Foundation

struct Product {
    var id: String
    var name: String
    var quantityPerItem: String
    var volume: String
    var price: Double
    var oldPrice: Double?
}
