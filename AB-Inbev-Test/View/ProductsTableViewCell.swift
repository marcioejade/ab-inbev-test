//
//  ProductsTableViewCell.swift
//  AB-Inbev-Test
//
//  Created by Marcio Izar Bastos de Oliveira on 28/08/20.
//  Copyright © 2020 Marcio Izar Bastos de Oliveira. All rights reserved.
//

import UIKit

class ProductsTableViewCell: UITableViewCell {

    @IBOutlet weak var plusButton: UIButton!
    
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var qtdItensToBuyTextField: UITextField!
    @IBOutlet weak var addButton: UIButton!
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var volumeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var oldPriceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        addButton.backgroundColor = .white
        addButton.layer.cornerRadius = 5
        addButton.layer.borderWidth = 1
        addButton.layer.borderColor = UIColor.bluePepsi.cgColor
        
        addButton.layer.shadowColor   = UIColor.black.cgColor
        addButton.layer.shadowOffset  = CGSize(width: 1.8, height: 1.8)
        addButton.layer.shadowRadius  = 0.4
        addButton.layer.shadowOpacity = 0.2

        addButton?.tintColor = .bluePepsi
        
        
        plusButton.backgroundColor = .white
        plusButton.layer.cornerRadius = 0.5 * plusButton.bounds.size.width
        plusButton.layer.borderWidth = 1
        plusButton.layer.borderColor = UIColor.bluePepsi.cgColor
        
        plusButton.layer.shadowColor   = UIColor.black.cgColor
        plusButton.layer.shadowOffset  = CGSize(width: 1.8, height: 1.8)
        plusButton.layer.shadowRadius  = 0.4
        plusButton.layer.shadowOpacity = 0.2
        
        plusButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        plusButton.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        plusButton?.tintColor = .bluePepsi
        
        
        minusButton.backgroundColor = .white
        minusButton.layer.cornerRadius = 0.5 * plusButton.bounds.size.width
        minusButton.layer.borderWidth = 1
        minusButton.layer.borderColor = UIColor.bluePepsi.cgColor
        
        minusButton.layer.shadowColor   = UIColor.black.cgColor
        minusButton.layer.shadowOffset  = CGSize(width: 1.8, height: 1.8)
        minusButton.layer.shadowRadius  = 0.4
        minusButton.layer.shadowOpacity = 0.2
        
        minusButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        minusButton.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        minusButton?.tintColor = .bluePepsi
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        if selected {
            contentView.backgroundColor = UIColor.white
        } else {
            contentView.backgroundColor = UIColor.white
        }
    }
    
    @IBAction func plusButtonToutched(_ sender: Any) {
        if var qtd = Int(qtdItensToBuyTextField.text ?? "0") {
            qtd += 1
            qtdItensToBuyTextField.text = qtd.description
        } else {
            qtdItensToBuyTextField.text = "1"
        }
    }
    
    @IBAction func minusButtonToutched(_ sender: Any) {
        if var qtd = Int(qtdItensToBuyTextField.text ?? "0") {
            qtd -= 1
            if qtd < 0 {
                qtd = 0
            }
            qtdItensToBuyTextField.text = qtd.description
            } else {
                qtdItensToBuyTextField.text = "0"
            }
    }
    

    @IBAction func addButtonToutched(_ sender: Any) {
        print("Add \(qtdItensToBuyTextField.text ?? "0") \(nameLabel.text ?? "")")
        changeColorAddButton()
    }
    
    func changeColorAddButton() {
        addButton.backgroundColor = .systemGray5
        addButton.layer.cornerRadius = 5
        addButton.layer.borderWidth = 1
        addButton.layer.borderColor = UIColor.clear.cgColor
        
        addButton.layer.shadowColor   = UIColor.clear.cgColor

        addButton?.setTitle("✓", for: UIControl.State.normal)
        addButton?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        addButton?.tintColor = .gray
    }
}
